import os
import random
import time
import speedtest

from modules.Content import Content
from modules.Station import Station

if __name__ == '__main__':

    #SIMULAÇÃO DO COMPORTAMENTO DE UMA REDE DE COMUNICAÇÃO MILITAR BASEADO NA MODELAGEM
    # Elementos: Dispositivo, Conteúdo (sigilo, precedência), Prescrição do emprego rádio

    ##CRIAÇÃO DOS NÓS DA REDE
    stations_list = []
    station1 = Station(1, "sta1", "192.168.0.1", "00:1B:C9:4B:E3:51", "Brigada Alpha")
    stations_list.append(station1)
    station2 = Station(2, "sta1", "192.168.0.2", "00:1B:C9:4B:E3:52", "Batalhão Charlie")
    stations_list.append(station2)
    station3 = Station(3, "sta1", "192.168.0.3", "00:1B:C9:4B:E3:53", "Companhia Xrey")
    stations_list.append(station3)
    station4 = Station(4, "sta2", "192.168.0.4", "00:1B:C9:4B:E3:54", "Companhia Yanque")
    stations_list.append(station4)
    station5 = Station(5, "sta3", "192.168.0.5", "00:1B:C9:4B:E3:55", "Companhia Zulu")
    stations_list.append(station5)
    station6 = Station(6, "sta1", "192.168.0.6", "00:1B:C9:4B:E3:56", "Regimento Bravo")
    stations_list.append(station6)
    station7 = Station(7, "sta1", "192.168.0.7", "00:1B:C9:4B:E3:57", "Esquadrão Romeu")
    stations_list.append(station7)
    station8 = Station(8, "sta2", "192.168.0.8", "00:1B:C9:4B:E3:58", "Esquadrão Golfe")
    stations_list.append(station8)

    ##RESTRIÇÕES DE COMUNICAÇÃO
    brigada_list = [2, 6] ### brigada só pode se comunicar com nós 2 e 6
    bat_charlie_list = [3, 4, 5, 6] ##
    com_xrey_list = [2, 4, 5] #
    com_yanque_list = [2, 3, 5] #
    com_zulu_list = [2, 3, 4] #
    reg_bravo_list = [1, 2, 7, 8] ##
    esq_romeu_list = [6, 8] #
    esq_golfe_list = [6, 7] #

    ##METADADOS DOS CONTEÚDOS
    precedence = ['urgente', 'urgentissima', 'preferencial', 'rotina']
    security_level = ['ultra-secreto', 'secreto', 'confidencial', 'reservado', 'ostensivo']

    ##PRESCRIÇÃO DO EMPREGO-RÁDIO
    classification = ['rad_sil_absoluto', 'rad_silencio', 'rad_restrito', 'radio_livre']

    #ESTRATÉGIA DE FORMA DE ONDA
    #st = speedtest.Speedtest()
    #print(st.download())
    #print(st.upload())

        ## LÓGICA OPERACIONAL DE TROCA DE MENSAGENS

    ## transmissões são realizadas durante um período de tempo
    ## se a comunicaçao for permitida vai receber uma resposta de "Ping realizado com sucesso."
    ## senao vai receber uma mensagem "Comunicaçao proibida para essa organização."
    count = 1
    list = []
    prec_rad_restrito_list = ['urgente', 'urgentissima']
    sender_bad_signal = [1,2,6]

    end_time = time.time() + 20
    #countTimer = 0
    #sleepTime = 0.500
    while time.time() < end_time:

        #CAPTURA NÍVEL DO SINAL
        resultado = os.popen("iwconfig wlp6s0 | grep -i --color Signal").read()
        resultado = resultado[43:]
        resultado = resultado[:3]
        resultado = int(resultado)


        hostname = "www.eb.mil.br"  # example

        sender = random.randrange(1, 8) # escolhe o sender aleatóriamente
        prec = random.choice(precedence) # escolhe a precedência aleatóriamente
        sec_level = random.choice(security_level) # escolhe o security_level aleatóriamente
        classif = random.choice(classification) # escolhe a classificação aleatóriamente

        if resultado < -67:
            sender = random.choice(sender_bad_signal) #SE O SINAL ESTIVER RUIM APENAS A BRIGADA E O REGIMENTO PODEM SE COMUNICAR
            print("Nível de sinal ruim. Serão transmitidas apenas mensagens da Brigada, Batalhão e Regimento. ", resultado , "dBm")
        else:
            print("Good signal ", resultado, "dBm")

        if classif == 'rad_restrito':
            prec = random.choice(prec_rad_restrito_list)

        content = Content(prec, sec_level)

        if sender == 1:
            receiver = random.choice(brigada_list)
        elif sender == 2:
            receiver = random.choice(bat_charlie_list)
        elif sender == 3:
            receiver = random.choice(com_xrey_list)
        elif sender == 4:
            receiver = random.choice(com_yanque_list)
        elif sender == 5:
            receiver = random.choice(com_zulu_list)
        elif sender == 6:
            receiver = random.choice(reg_bravo_list)
        elif sender == 7:
            receiver = random.choice(esq_romeu_list)
        elif sender == 8:
            receiver = random.choice(esq_golfe_list)

        s = [n for n in stations_list if n.id == sender]
        r = [n for n in stations_list if n.id == receiver]
        for i in s:
            sender_id = i.id
            sender = i.military_organiation

        for i in r:
            receiver_id = i.id
            receiver = i.military_organiation

        if classif == 'rad_sil_absoluto':
            print("Rádio em silêncio absoluto (3 seg)\n")
            time.sleep(3)
        elif classif == 'rad_silencio':
            print("Rádio em silêncio. Permitida apenas escuta (3 seg) \n")
            time.sleep(3)
        elif classif == 'rad_restrito':
            print("Rádio restrito. Permitida apenas mensagens urgentes e urgentíssimas.")
            if prec == "urgente" or prec == 'urgentissima':
                print('Sender:', sender)
                print('Receiver:', receiver)
                response = os.system("ping -c 1 " + hostname)
                print('\n')
                print("Precedência:", prec, "Security level: ", sec_level, "\n")

        elif classif == 'radio_livre':  # Verifica se obedece às restrições
            print("Rádio livre.")
            print('Sender:', sender)
            print('Receiver:', receiver)
            #count = count + 1
            response = os.system("ping -c 1 " + hostname)
            print('\n')
            print(" Precedência:", prec, "Security level: ", sec_level, "\n")


###----------------------------------------------------------------------------