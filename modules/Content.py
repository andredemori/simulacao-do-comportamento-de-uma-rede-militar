class Content:
    def __init__(self, precedence, security_level):
        self.precedence = precedence # urgente, urgentíssima, preferencial, rotina
        self.security_level = security_level # ultra-secreto, secreto, confidencial, reservado, ostensivo